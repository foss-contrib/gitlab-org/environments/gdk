# GDK Environment [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)


This repository is home to a Dockerfile that puts together everything needed for<br />
contributing to GitLab; All that is necessary is for one to launch a container<br />
using the provided Dockerfile and the container instance provides everything<br />
one needs to test all aspects of a running GitLab instance.

## Table of Contents

* [Prerequisites](#prerequisites)
  * [Configuring Hosts](#setting-local-hosts)
  * [Install Docker](#install-docker)
* [How to Build the Environment](#how-to-build-the-environment)
  * [Space Requirements](#space-requirements)
  * [Optional Build Parameters](#optional-parameters)
* [How to Run the Environment](#how-to-run-the-environment)
  * [Using Docker CLI](#using-docker-cli)
  * [Using Docker Compose](#using-docker-compose)
  * [Run the Local Image](#local-image)
  * [Run the Public Image](#public-pre-built-image)
* [Using GDK](#using-gdk)
  * [Logs](#logs)
  * [Updates](#updating-gdk)
  * [Tinkering with GDK Manually](#manual-tinkering)
  * [Extras](#extras)

## Prerequisites

The prerequisites for this Docker container environment are contingent on whether<br />
you are building and using the resulting image, or only using the publicly <br />
available image.

### Setting Local Hosts

Whether you are intending to use the publicly available image or not, you need<br />
to configure the appropriate local hosts entry for `gdk.test` *(recommended)*.

On Debian Linux it would be done as follows:

```bash
/bin/bash -c 'echo "$user_pass"' | \
sudo -S /bin/bash -c 'echo -e "\n127.0.0.1	gdk.test" >> /etc/hosts'
```

You can do it manually with `sudo nano /etc/hosts` as well, simply following<br />
the formatting of the existing entries. This holds true for Mac OS[X] as well.

The same can be done on Windows, manually, at the proper location, typically<br />
`c:\Windows\System32\Drivers\etc\hosts`. You'll need to open Notepad<br />
or Notepad++ as an Administrator first, then use the "Open" menu to browse to<br />
the respective directory - or you'll find you don't have permissions to save the<br />
file after using the *"Open with..."* context menu.

### Install Docker

You'll need to install, minimally:

* Docker CE (Container Engine)
* Docker CLI
* Containerd.io

Those tools are the minimum needed for building and running Docker images on<br />
your local host. Official instructions for getting them all installed are [here](https://docs.docker.com/engine/install/),<br />
or you may follow along with my own notes, [here](https://gitlab.com/-/snippets/1829066) - just know that I've since<br />
installed Docker Desktop for Linux (Debian); It makes managing image volumes<br />
fairly easy. Steps to following through with that after going through my notes<br />
can be followed [here](https://docs.docker.com/desktop/install/debian/).

If you visit the official link, though, just follow the links under **Platform**,<br />
and keep navigating for your specific flavor (linux) or install (Mac/Windows).

## How to Build the Environment

To build the environment, clone the repository:

```bash
git clone https://gitlab.com/foss-contrib/gitlab-org/environments/gdk
```

Change directories to that of the `docker-files `<br />
directory, and run the following commands:

```bash
cd gdk/docker-files
```

Finally, build the image:

```bash
docker build --tag=gdk:debian-slim .
```

The build will certainly take a while - you may want to ensure you have enough<br />
space in the docker volumes directory that is used for your images (i.e.):

```bash
sudo du -sh /var/lib/docker
```

### Space Requirements

If you need more space, just remove some containers you aren't running right now<br />
and don't mind starting again from a fresh pull; even remove the images they use.

You could do a system prune to remove all stopped containers and dangling images<br />
as well:

```bash
docker system prune --all --volumes
```

### Optional Parameters

You can set a custom user password by supplying a `--build-arg` in the form of<br />
of `user_pass=<password>`. The default password is `password`, when none is<br />
provided.

```bash
docker build --build-arg user_pass=password --tag=gdk:debian-slim .
```

The user mentioned is a *system* user named 'gitlab', and the initial
working directory is `$HOME/workspaces/gitlab-development-kit`.

The `root` user, as per Docker conventions, has no password. It's not recommended<br />
to leverage this image as the root user; so if you do so, you may run into issues<br />
(you'll have to modify the Dockerfile or add another layer with the right <br />
`USER root` directive).

The resulting image should be visible in your images list:

```bash
docker images
```

[![Resulting Image](https://gitlab.com/foss-contrib/gitlab-org/environments/gdk/-/raw/main/assets/image-list-post-docker-build.png)](#how-to-build-the-environment)

## How to Run the Environment

Assuming you build the image in the last step, you can run the image as you<br />
would any other Docker image:

### Using Docker CLI

```bash
docker run -d -p 3723:443 --add-host gdk.test:127.0.0.1 --name gdk-debian-slim gdk:debian-slim
```

Please note the `--add-host` option. It's an optional parameter to Docker CLI's<br />
`run` command, but it's required for our purposes. Attempting to add a hosts entry<br />
during build results in the entry being overwritten - so it must be added at<br />
run-time.

The host that is [recommended by GitLab for use with GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#set-up-gdktest-hostname) is `gdk.test`, and as such<br />
the specific option value one should provide is specifically:

```bash
--add-host gdk.test:127.0.0.1
```

### Using Docker Compose

You could also opt to leverage Docker Compose (my recommendation), either:<br />

* Manually
  [Docker Compose](https://docs.docker.com/compose/install/linux/#install-the-plugin-manually):
* Or Automatically (more recently introduced and recommended):
  [Docker Compose Repository](https://docs.docker.com/compose/install/linux/#install-using-the-repository)

Or, alternatively, you can also install Docker Desktop and get Compose as a part<br />
of it (yes, even now officially on Debian/Ubuntu/Linux variants, no longer beta):

* [Docker Desktop for Linux (Debian)](https://docs.docker.com/desktop/install/debian/)

Just be sure that if you use Docker Desktop, that you go into **settings** ⇨ <br />
**Docker Engine** ⇨ and in the JSON editor to the right you modify the **featuers**<br />
section so that `buildKit: true` is set to `buildKit: false`:

```jsonc
{
  // ...
  "features": {
    "buildkit": false
  }
}
```

With Docker Compose, orchestrating half or full stacks with docker images is<br />
extremely easy to do, and so subsequently running and managing them.

A compose (`docker-compose.yml`) file is provided for whether you want to run a<br />
locally built version of the gdk image, or a public pre-built image (not yet <br />
available ⇨ coming soon!)

### Local Image

Head back over to the root of the project, change directories into<br />
`docker-compose/local` - and run the following command(s):

```bash
cd ../docker-compose/local
docker-compose -d gdk-debian-slim
```

Once available (presuming its hosted in the repository's container registry), you<br />
can use the pre-built public image.

### Public Pre-built Image

A preexisting image wil exist at the public GitLab repository registry that hosts<br />
this Docker solution. To use that image instead of building it yourself, run<br />
the following to pull the image in advance yourself - Docker Compose will pull it<br />
for you otherwise; So this part may be skipped:

```bash
docker pull https://registry.gitlab.com/foss-contrib/gitlab-org/environments/gdk/gdk:debian-slim
```

Ultimately, follow the same instructions from the root of the repository, only<br />
change directories into the root `docker-compose` directory instead (assuming<br />
you didn't build the imate after just cloning the repository):

```bash
cd gdk/docker-compose
docker-compose -d gdk-debian-slim
```

The `gdk.test` host entry is handled by the compose file for us, so it does not<br />
need to be supplied.

## Using GDK

Regardless of how you chose to run the image, once running You should be able to<br />
access a running gdk instance at `https://gdk.test:3723`.

You'll need to *"proceed anyways"* in chrome when you get the SSL certificate<br />
error; You'll see the respective link upon clicking the **"Advanced"** button.

On Mac this can be problematic, but if using Chrome you can still bypass the<br />
issue by following the instructions found [here](https://stackoverflow.com/questions/58802767/no-proceed-anyway-option-on-neterr-cert-invalid-in-chrome-on-macos).

The default username and password for logging into the GDK instance through the<br />
web portal is as [documented](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/gdk_commands.md#get-the-login-credentials):

* Username is `root`
* Password is `5iveL!fe`

These credentials can also be found by running the command `gdk help` from an<br />
interactive shell *(see [Tinkering with GDK Manually](#manual-tinkering) and*<br />
*[Extras](#extras))*.

After logging in for the first time you'll be asked to change the password.

### Logs

Aside from that, all [GDK commands](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/gdk_commands.md) are usable from within an interactive shell<br />
as you would expect them to be. Logs can be found at:

* `/var/log/gdk-nginx-access.log`
* `/var/log/gdk-nginx-error.log`

All `gdk` output is put to **stdout** and **stderr**, with nginx configured to<br />
redirect all output to `/dev/stdout` and `/dev/stderr`. With both components set<br />
to output to the same standard outputs - the singular custom log redirect provides<Br />
a convenient single point of reference for all logging..

### Updating GDK

For major update points you'll find a new image or updated repository to pull or<br />
build from. When all else fails, you can manage things manually.

### Manual Tinkering

Managing GDK from this point by hand should be done through interactive shells, <br />
and in that sense, with the help of official documentation (used in building <br />
this container image):

* [Official Usage Instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/index.md)
* [Official Install Instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#install-and-configure-gdk)
* [Official Update Instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/gdk_commands.md#update-gdk)

When running any commands using `sudo`, the default password is `password`. You<br />
had the option of changing this if you chose to build the image yourself locally<br />
and provided an alternate to `--build-arg user_pass=<alternate>`.

### Extras

For an easy method of fetching an interactive shell from a Docker container, you<br />
can borrow the `docker-helper.sh` script provided in this repository if you are<br />
using Linux (and possibly Mac):

* In a terminal change directories to `bin`
  * `cd ../bin`
* Mark the helper file as executable and alias (symbolically link) it in <br />
  `/usr/local/bin`:
  ```bash
  chmod +x ./docker-helper.sh
  sudo ln -s /full/path/to/docker-helper.sh /usr/local/bin/docker-helper
  ```
* For any container you run, you can get either the ip for the container, or its<br />
  interactive shell, by providing the container's name to `docker-helper` as<br />
  follows:
  * For the interactive shell:
    ```bash
    docker-helper get-shell --name gdk-debian-slim
    ```
  * For the IP Address:
    ```bash
    docker-helper get-ip --name gdk-debian-slim
    ```
  * In the former case, you'll be brought into the context of the containers' <br />
    terminal - whereas in the latter the IP Address will be printed in the terminal.

# License Information

For licensing information, please defer to the LICENSE.md file in the root of<br />
this repository.

---

[![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)