##
# SPDX-PackageName: GDK Development Environment
# SPDX-FileCopyrightText: ⓒ 2015 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
# SPDX-License-Identifier: MIT
##

## http://gdk.test server configuration
#
server {
    listen 80 http2;
    listen [::]:80 http2;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ## Host declaration
    server_name gdk.test;

    ## Remove NGINX version string
    server_tokens off;

    ## Standardize server behavior
    chunked_transfer_encoding off;
    add_header Accept-Ranges bytes;

    ## SSL Configuration
    #ssl on;
    # https://wiki.debian.org/Self-Signed_Certificate
    ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
    ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;
    ssl_buffer_size 8k;

    ## sudo openssl dhparam -out ~/workspaces/docker/mmod-co/production/dh-param/dhparam-2048.pem 2048
    #ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols SSLv3 TLSv1.3 TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;
    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    #ssl_ecdh_curve secp384r1;
    #ssl_session_tickets off;

    # OCSP stapling
    #ssl_stapling on;
    #ssl_stapling_verify on;
    #resolver 8.8.8.8 8.8.4.4;

    root /var/www/gdk;
    index index.html;

    location / {
        alias /var/www/gdk;
        gzip off;

        # Utilize nginx as a reverse-proxy for the gdk's ruby application:
        #Accept-Ranges bytes;
        add_header Access-Control-Allow-Origin  *;
        #add_header Access-Control-Allow_methods GET,POST,PUT,DELETE,OPTIONS;
        add_header Access-Control-Allow_headers Content-Type;

        proxy_http_version  1.1;
        proxy_set_header    Upgrade $http_upgrade;
        proxy_set_header    Connection 'upgrade';
        proxy_set_header    X-Real-IP $remote_addr;
        #proxy_set_header    X-Forwarded-For $remote_addr;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        #proxy_set_header    Host $host;
        proxy_set_header    Host $http_host;
        proxy_set_header    X-Forwarded-Proto $scheme;
        proxy_cache_bypass  $http_upgrade;
        proxy_pass          $scheme://127.0.0.1:3000;

        # Any special catches for the root domain go here:
        location ~* \.(?:ico|css|js|gif|jpe?g|png|ttf|woff)$ {
            alias /var/www/gdk;
            access_log off;
            expires 30d;
            add_header Pragma public;
            add_header Cache-Control "public, mustrevalidate, proxy-revalidate";
            #proxy_buffering         off;
            proxy_http_version      1.1;
            proxy_pass              $scheme://127.0.0.1:3000;
        }
    }

    # Subdomains, without using subdomains :)
    location /cdn {
        alias /var/www/cdn;
        #rewrite ^/cdn(.*)$ $1 last;

        # Style our directory listings
        #add_before_body /../nginx/nginx-before.txt;
        #add_after_body /../nginx/nginx-after.txt;

        # Needed to serve directories with out an index files
        #autoindex on;

        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ $uri/index.html?$args;
    }

    # Disable access logs for robots.txt
    location = /robots.txt { access_log off; log_not_found off; }
    #location = /favicon.ico { access_log off; log_not_found off; }

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    location ~ /\.ht {
       deny all;
    }
}