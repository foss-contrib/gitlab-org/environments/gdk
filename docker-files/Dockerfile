##
# SPDX-PackageName: GDK Development Environment
# SPDX-FileCopyrightText: ⓒ 2015 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
# SPDX-License-Identifier: MIT
##

##
# run this with $> docker build --build-arg user_pass=<password> --tag=<tag> .
##
# could be... as intermediate
FROM debian:bookworm-slim
#FROM bookworm

# must come after every 'FROM'; (yes repeat if needed in later stages)
ARG user_pass=password

LABEL maintainer="Richard Winters"
LABEL website.name="GDK Docker Image Public Repository"
LABEL website="https://gitlab.com/foss-contrib/gitlab-org/environments/gdk"
LABEL desc="This image can be used to spawn a GDK instance container, allowing \
one to undertake GitLab developer (i.e. to contribute) without having to have a \
full-blown gdk instance configured on their home system."

#RUN useradd --create-home --shell /bin/bash gitlab; \
# Best way to echo build-arg pass to chpassword (works, quote entire command)
#    /bin/bash -c 'echo "gitlab:$user_pass" | chpasswd'; \
# this also works, but might not be bash
##    echo "gitlab:$user_pass" | chpasswd; \
# this does NOT work
##    /bin/bash -c echo "gitlab:$userpass" | chpasswd; \
#    adduser gitlab sudo

#RUN echo "Password: $user_pass"

##
# STAGE 1 - MAJOR DEPS AND USER CREATION
##

RUN apt-get update && apt-get upgrade -y && \
# prep tools also for asdf (last 2)
    apt-get install -y curl git \
        software-properties-common \
        gnupg2 apt-transport-https \
# prep deps for asdf-ruby
        build-essential autoconf \
        bison patch rustc \
        libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libgmp-dev \
        libncurses5-dev libffi-dev libgdbm6 libgdbm-dev libdb-dev uuid-dev \
# prep deps for asdf-nodejs
        dirmngr gawk \
# prep deps for gdk
        graphicsmagick exiftool runit libkrb5-dev libgpgme-dev libre2-dev \
        libpcre2-dev libpcre3-dev libsqlite3-dev libcurl4-gnutls-dev sqlite3 \
        cmake pkg-config postgresql postgresql-contrib \
        ssl-cert nginx supervisor \
# prep deps for non-root user
        sudo; \
# create special user
    useradd --create-home --shell /bin/bash gitlab; \
# Best way to echo build-arg pass to chpassword (works, quote entire command)
    /bin/bash -c 'echo "gitlab:$user_pass" | chpasswd'; \
# this also works, but might not be bash
#    echo "gitlab:$user_pass" | chpasswd; \
# this does NOT work
#    /bin/bash -c echo "gitlab:$userpass" | chpasswd; \
    adduser gitlab sudo


##
# STAGE 2 - ASDF DERIVED DEPS AND CONFIGURATION
##

## Change user
USER gitlab

# change to it for all subsequent commands
WORKDIR /home/gitlab

# install asdf
RUN \
    # configure git to get rid of detached head warnings
    git config --global advice.detachedHead false; \
    git clone https://github.com/asdf-vm/asdf.git $HOME/.asdf --branch v0.10.2; \
#    chmod +x ~/.asdf/asdf.sh; \
    /bin/bash -c 'echo -e "\n\n## Configure ASDF \n. $HOME/.asdf/asdf.sh" >> ~/.bashrc'; \
    /bin/bash -c 'echo -e "\n\n## ASDF Bash Completion: \n. $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc'; \
    exec bash; \
# install asdf-ruby
    /bin/bash -c asdf plugin add ruby https://github.com/asdf-vm/asdf-ruby.git; \
# install asdf-nodejs
    /bin/bash -c asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git


##
# STAGE 3 - GDK DERIVED DEPS
##

# create a special directory for gdk setup
RUN mkdir -p ~/workspaces

# change to it for all subsequent commands
WORKDIR /home/gitlab/workspaces

# clone gdk
RUN \
    git clone https://gitlab.com/gitlab-org/gitlab-development-kit.git


##
# STAGE 4 - GDK BOOTSTRAP
##

# change to it for all subsequent commands
WORKDIR /home/gitlab/workspaces/gitlab-development-kit

# bootstrap gdk dependencies for install
RUN \
# run a sudo command so we sudo prepped with password for gdk install
# "RUNNING AS ROOT IS NOT SUPPORTED" [REMOVE]
#    /bin/bash -c 'echo $user_pass | sudo -S make bootstrap'; \
# "SUDO GDK COMMAND NOT FOUND" [REMOVE]
#    /bin/bash -c 'echo $user_pass | sudo -S gdk install'; \
    make bootstrap; \
# bootstrap suggests to: [TODO: REMOVE]
    /bin/bash -c 'source "$HOME/.asdf/asdf.sh"'; \
# restart bash again to ensure gdk commands, we had issues with asdf:
# [TODO: REMOVE]
    exec bash;


##
# STAGE 5 - GDK PRECONFIG AND INSTALL WITH SYSTEM HOSTS TWEAK
##

# install gdk (with last-minute recommended changes)
RUN \
# Here we combine a source with gdk install, it works to ensure gdk commands are
# available where the previous individual source and restart fail for this step
# even though it is still the only solution for the asdf step.
    /bin/bash -c 'source "/home/gitlab/.asdf/asdf.sh" && gdk install'; \
# add gdk.test to /etc/hosts -
# IMPORTANT NOTE 1:
#  YES, THIS MUST BE DONE ON THE HOST THE CONTAINER IS RUNNING ON AS WELL!
# IMPORTANT NOTE 2:
#  WE CAN LIKELY REMOVE THIS AS WE CAN START A CONTAINER WITH --add-host option:
# `docker run -d -p 3723:443 --add-host gdk.test:127.0.0.1 --name <name> <image>`
#  HOWEVER, BE CAREFUL MODIFYING THIS LINE, THERE'S A TAB BETWEEN THE HOST ENTRY
#  FOR A REASON - MAKE VERY SURE YOU DO NOT MESS THIS UP! IT'S POSSIBLE THAT WE
#  DO NEED TO KEEP THIS FOR gdk.test TO RESOLVE DURING RECONFIGURE, ALTHOUGH IT
#  DOES NOT PERSIST TO THE RUNNING CONTAINER - REQUIRING STILL --add-host option
    /bin/bash -c 'echo "$user_pass"' | sudo -S /bin/bash -c 'echo -e "\n127.0.0.1	gdk.test" >> /etc/hosts'; \
# ensure test domain added to gdk configuration (usually empty, but play safe)
# while were add it let's set up ssl, as we're unable to connect any way except
# by using https from our real host, mapped to 443 on the container, proxy passed
# to the gdk instance...
    /bin/bash -c 'echo -e "\nhostname: gdk.test\nhttps:\n  enabled: true" >> gdk.yml'; \
# now stop, reconfigure, and restart - gdk
#
# run install with out the updated configuration (if done first we run into issues
    /bin/bash -c 'source "/home/gitlab/.asdf/asdf.sh" && gdk stop && gdk reconfigure';


##
# STAGE 6 - NGINX CONFIGURATION
##

# configure nginx
ADD gdk.test /etc/nginx/sites-available/gdk-test

RUN \
    /bin/bash -c 'echo $user_pass | sudo -S ln -s /etc/nginx/sites-available/gdk-test /etc/nginx/sites-enabled/gdk-test'; \
    /bin/bash -c 'echo $user_pass | sudo -S mkdir -p /var/www/gdk';


##
# STAGE 7 - CLEANUP INSTALL AND GENERAL STDOUT/STEDERR REDIRECTION
##

# Separate anything more from the last command; it's huge and takes a long time!
RUN \
# check log dir
#    ls /var/log; \
# clean up install
    /bin/bash -c 'echo $user_pass | sudo -S apt-get autoremove'; \
    /bin/bash -c 'echo $user_pass | sudo -S apt-get clean'; \
    /bin/bash -c 'echo $user_pass | sudo -S rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*'; \
# set daemon off on nginx config so we control its state
    /bin/bash -c 'echo "$user_pass"' | sudo -S /bin/bash -c 'echo "daemon off;" >> /etc/nginx/nginx.conf'; \
# set up logging (stdout and std error will go to /var/log/gdk-nginx-*.log, the
# naming convention similar to nginx log convention; with nginx logging redirected
# to /dev/stdout and /dev/stderr via supervisord - for a combined log considering
# the combined nature of our container (both nginx and gdk)).
    /bin/bash -c 'echo $user_pass | sudo -S ln -sf /dev/stdout /var/log/gdk-nginx-access.log'; \
    /bin/bash -c 'echo $user_pass | sudo -S ln -sf /dev/stderr /var/log/gdk-nginx-error.log';
    #source "/home/gitlab/.asdf/asdf.sh";


##
# STAGE 8 - EXPOSE PORTS
##

# does not publish, acts as documentation on what's expected
# mapping of ports is still requierd in run command (or compose configuration)
#
# 3000 is internal, we can't hit the gdk instance from it externally - not even
# if its mapped to another host port
#EXPOSE 3000/tcp
#EXPOSE 3000/udp
# it seems we have to setup nginx to reverse proxy to an actual host (domain), and
# it can either be http (which didn't work, even when configured that way)
#EXPOSE 80/tcp
#EXPOSE 80/udp
# or https (as this image is configured)
EXPOSE 443/tcp
EXPOSE 443/udp


##
# STAGE 9 - DAEMONIZE NGINX AND SUBSEQUENT CONTAINER
##

# ensures a start of nginx  upon container start
ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# add entrypoint script
ADD start-container /usr/local/bin/start-container

# make entrypoint script executable
RUN /bin/bash -c 'echo $user_pass | sudo -S chmod +x /usr/local/bin/start-container'

# declare entrypoint
ENTRYPOINT [ "start-container" ]