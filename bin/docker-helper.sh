#!/bin/bash
##
# SPDX-PackageName: GDK Development Environment
# SPDX-FileCopyrightText: ⓒ 2015 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
# SPDX-License-Identifier: MIT
##

##
# docker-helper is a simple docker command manager
#
# Example usage:
#
# ./docker-helper get-shell --name gdk_debian
#
# ./docker-helper get-ip --name gdk_debian
#
# There is no validation, help, or indicator that alerts to failed commands!
##

# Standard bash scripting stuff:
COMMAND=""
EXEC1=""
EXEC2=""
EXEC3=""
EXECPARTS=1
NAME=""
ID=""

# Handling options:
while [ -n "$1" ]; do # while loop starts
    case "$1" in

    --id)
         echo "Container id is '${2}'"
         ID="${2}"

           shift # Here we shift because we want to shift 2
                 # spaces in total before the next iteration

         ;;

    --name)
           echo "Container name is '${2}'"
           NAME="${2}"

           shift # Here we shift because we want to shift 2
                 # spaces in total before the next iteration

           ;;

    get-ip)
          COMMAND="${1}"
          EXEC1="docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'"

          ;;

    get-shell)
          COMMAND="${1}"
          EXECPARTS=2
          EXEC1="docker exec -it"
          EXEC2="/bin/bash"

          ;;

    *) echo "Option $1 not recognized" ;;

    esac

    # Here we use shift which moves $2 --> $1 so that variables/options always
    # contain the next set of option/value pairs in each new iteration.
    shift

done


# Handling parameters:
for param in "$@"; do
    echo  "Param: ${param}"
done


# Run command:
if [ "$COMMAND" == "" ]; then
    echo
    echo "## Missing command..."
    echo
else
        echo
        echo "## Executing '$COMMAND'..."
        echo
    if [ $EXECPARTS == 1 ]; then
        if [ "$ID" == "" ]; then  # ID takes precedence
            eval "$EXEC1 $NAME"
        else
            eval "$EXEC1 $ID"
        fi
    else
        if [ $EXECPARTS == 2 ]; then
            if [ "$ID" == "" ]; then  # ID takes precedence
                eval "$EXEC1 $NAME $EXEC2"
            else
                eval "$EXEC1 $ID $EXEC2"
            fi
        fi
    fi
fi